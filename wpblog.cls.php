<?php
class wpblog{
	private $category=false;
	private $myarray = array();
	private $currentKey = false;
	private $username=false;
	private $password=false;
	private $blogrpcURL=false;
	private $xmlRequest=false;
	private $post_type = 'job_listing';
	private $xmlResponse=false;
	private $showResponse=false;
	private $j=-1;
	private $aut_user="wpnewslisting";
	private $aut_pass="newslisting2016";
	public function __construct($username,$password,$blogrpcURL){
		$this->username=$username;
		$this->password=$password;
		$this->blogrpcURL=$blogrpcURL;
	}
	
	public function setLimit($limit){
		if(is_numeric($limit)){
			$this->blogLimit=$limit;
		}
	}
	public function getBlogCategories(){
		if($this->username!==false && $this->password!==false){
			$this->xmlRequest='';
			$this->xmlRequest.='<methodCall>';
			$this->xmlRequest.='<methodName>wp.getTerms</methodName>';
			$this->xmlRequest.='<params>';
			$this->xmlRequest.='<param><value>all</value></param>';
			$this->xmlRequest.='<param><value>'.$this->username.'</value></param>';
			$this->xmlRequest.='<param><value>'.$this->password.'</value></param>';
			$this->xmlRequest.='<param><value>job_listing_category</value></param>';
			$this->xmlRequest.='</params>';
			$this->xmlRequest.='</methodCall>';	
			$this->processRequest();
			return $this->parseXML();
			
		}	
	}

    public function getAllTermsOfAllTaxonomies(){
		if($this->username!==false && $this->password!==false){
			$this->xmlRequest='';
			$this->xmlRequest.='<methodCall>';
			$this->xmlRequest.='<methodName>wp.getAllTermsOfAllTaxonomies</methodName>';
			$this->xmlRequest.='<params>';
			$this->xmlRequest.='<param><value>all</value></param>';
			$this->xmlRequest.='<param><value>'.$this->username.'</value></param>';
			$this->xmlRequest.='<param><value>'.$this->password.'</value></param>';
			$this->xmlRequest.='</params>';
			$this->xmlRequest.='</methodCall>';	
			$this->processRequest();
            $arr1=array();
            $arr2=array();
			$arr1=$this->parseXML();
            $cnt=count($arr1);
            for($i=0;$i<$cnt;$i++){
                $arr2[$arr1[$i]["taxonomy"]][]=$arr1[$i];
            }
            return $arr2;
		}	
	} 


	public function getBlogDetails()
	{
		if($this->username!==false && $this->password!==false){
			$this->xmlRequest='';
			$this->xmlRequest.='<methodCall>';
			$this->xmlRequest.='<methodName>wp.getPosts</methodName>';
			$this->xmlRequest.='<params>';
			$this->xmlRequest.='<param><value>all</value></param>';
			$this->xmlRequest.='<param><value>'.$this->username.'</value></param>';
			$this->xmlRequest.='<param><value>'.$this->password.'</value></param>';
			$this->xmlRequest.='<param>
									<value>
										
												<struct>
													<name>post_type</name>
													<value>'.$this->post_type.'</value>
													<name>number</name>
													<value>5</value>
													<name>offset</name>
													<value>0</value>
													<name>orderby</name>
													<value>post_date_gmt</value>
													<name>order</name>
													<value>desc</value>			
												</struct>
											
									</value>	
								</param>';	
											



			
			$this->xmlRequest.='</params>';
			$this->xmlRequest.='</methodCall>';	
			$this->processRequest();
			return $this->parseXML();
			//return $this->processRequestBlog();			
			
		}	
	}

		public function getBlogMetaDetails($blogID)
	{
		if($this->username!==false && $this->password!==false){
			$this->xmlRequest='';
			$this->xmlRequest.='<methodCall>';
			$this->xmlRequest.='<methodName>metaWeblog.getPost</methodName>';
			$this->xmlRequest.='<params>';
			$this->xmlRequest.='<param><value>'.$blogID.'</value></param>';
			$this->xmlRequest.='<param><value>'.$this->username.'</value></param>';
			$this->xmlRequest.='<param><value>'.$this->password.'</value></param>';
			$this->xmlRequest.='</params>';
			$this->xmlRequest.='</methodCall>';	
			return $this->processRequestBlog();
			
			
		}	
	}

	function getBlogsByCategory(){
		if($this->category!==false && $this->username!==false && $this->password!==false){
			$this->xmlRequest='';
			$this->xmlRequest.='<methodCall>';
			$this->xmlRequest.='<methodName>wp.getCategoryBlogs</methodName>';
			$this->xmlRequest.='<params>';
			$this->xmlRequest.='<param><value>'.$this->username.'</value></param>';
			$this->xmlRequest.='<param><value>'.$this->password.'</value></param>';
			$this->xmlRequest.='<param><value>'.$this->category.'</value></param>';
			$this->xmlRequest.='<param><value>'.$this->blogLimit.'</value></param>';
			$this->xmlRequest.='<param><value>'.$this->orderBy.'</value></param>';
			$this->xmlRequest.='</params>';
			$this->xmlRequest.='</methodCall>';	
			$this->processRequest();
			return $this->parseXML();
		}
		return false;
	}
	function getChildPage($include=""){
		if($this->category!==false && $this->username!==false && $this->password!==false){
			$this->xmlRequest='';
			$this->xmlRequest.='<methodCall>';
			$this->xmlRequest.='<methodName>wp.getChildPage</methodName>';
			$this->xmlRequest.='<params>';
			$this->xmlRequest.='<param><value>'.$this->username.'</value></param>';
			$this->xmlRequest.='<param><value>'.$this->password.'</value></param>';
			$this->xmlRequest.='<param><value>'.$this->category.'</value></param>';
			$this->xmlRequest.='<param><value>'.$this->blogLimit.'</value></param>';
			$this->xmlRequest.='<param><value>'.$include.'</value></param>';
			$this->xmlRequest.='<param><value>'.$this->orderBy.'</value></param>';
			$this->xmlRequest.='</params>';
			$this->xmlRequest.='</methodCall>';	
			$this->processRequest();
			return $this->parseXML();
		}
		return false;
	}
	
	function getNavMenu(){
		$this->xmlRequest='';
		$this->xmlRequest.='<methodCall>';
		$this->xmlRequest.='<methodName>wp.getMenu</methodName>';
		$this->xmlRequest.='</methodCall>';	
		$this->showResponse=true;
		$this->processRequest(true);
		return $this->parseMenu();
	}
	
	function getCategoryInfo(){
		$this->xmlRequest='';
		$this->xmlRequest.='<methodCall>';
		$this->xmlRequest.='<methodName>wp.getCategoryInfo</methodName>';
		$this->xmlRequest.='<params>';
		$this->xmlRequest.='<param><value>'.$this->category.'</value></param>';		
		$this->xmlRequest.='<param><value>'.$this->username.'</value></param>';
		$this->xmlRequest.='<param><value>'.$this->password.'</value></param>';

		$this->xmlRequest.='</params>';
		$this->xmlRequest.='</methodCall>';	
		$this->processRequest();
		return $this->parseXML();
	}
	function processRequest($loadXML=true){
		if($this->xmlRequest!==false && $this->blogrpcURL!==false){
		    
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $this->blogrpcURL);
			curl_setopt($curlHandle, CURLOPT_HEADER, false);
			curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
			curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $this->xmlRequest);
			if($this->aut_user !== false && $this->aut_pass !== false){

				curl_setopt($curlHandle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
				curl_setopt($curlHandle, CURLOPT_USERPWD, $this->aut_user . ":" . $this->aut_pass);
			}	
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER,1);
			/*Now execute the CURL, download the URL specified*/
			$response = curl_exec($curlHandle);
			/*echo $response;exit;
			   $myfile = fopen("listing-live.xml", "w") or die("Unable to open file!");
					fwrite($myfile, $response);
					fclose($myfile);*/
			if($response!==false && strpos("faultCode",$response)===false && $loadXML){
			    $response = str_replace('<?xml version="1.0" encoding="UTF-8"?>', "", $response);
			    $response = str_replace('dateTime.iso8601', "string", $response);
			     //echo $response;exit;
				$response=simplexml_load_string($response);
				$this->xmlResponse=$response;
			}else{
				$this->xmlResponse=$response;
			}
		}	
	}


 function generateArray($arr){
	
		foreach($arr as $x=>$y){
		if(is_array($y)){
			if($this->currentKey=="custom_fields"){
				$this->getCustomFields($y);
				$this->currentKey="";
			}
else if($currentKey=="post_thumbnail"){
				getThumbnail($y);
				if(!isset($myarray[$this->j]['post_thumbnail'])){
					$myarray[$this->j]['post_thumbnail']="";
				}
				$currentKey="";
			}
			else{
				$this->generateArray($y);
			}	
		}else{
			if(strtolower(trim($x)) == 'name'){
				if($y=="post_id"){
					$this->j++;
				}
				$this->currentKey=$y;
			}else{
				$this->myarray[$this->j][$this->currentKey]=$y;
				$this->currentKey="";
			}
		}
		
	}
	
}

function getThumbnail($arr){
	
	foreach($arr as $key=>$value){
		if($value == "link"){
			$myarray[$this->j]['post_thumbnail']=$arr["value"]["string"];
		}else{
			if(is_array($value)){
				getThumbnail($value);
			}
		}	
	}
}

function getCustomFields($arr){
	
	$newArray=$arr["array"]["data"]["value"];
	for($i=0;$i<count($newArray);$i++){
		list($key, $value) = each($newArray[$i]['struct']["member"][1]["value"]);
		list($datakey,$datavalue)=each($newArray[$i]['struct']["member"][2]["value"]);
		$this->myarray[$j][$value]=$datavalue;		
	}
}

		function processRequestBlog($loadXML=true){
		if($this->xmlRequest!==false && $this->blogrpcURL!==false){
		    
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $this->blogrpcURL);
			curl_setopt($curlHandle, CURLOPT_HEADER, false);
			curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
			curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $this->xmlRequest);
			if($this->aut_user !== false && $this->aut_pass !== false){

				curl_setopt($curlHandle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
				curl_setopt($curlHandle, CURLOPT_USERPWD, $this->aut_user . ":" . $this->aut_pass);
			}	
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER,1);
			/*Now execute the CURL, download the URL specified*/
			$response = curl_exec($curlHandle);
			 //echo $response;exit();
			//var_dump($response);
			
			    $response = str_replace('<?xml version="1.0" encoding="UTF-8"?>', "", $response);
			    $response = str_replace('dateTime.iso8601', "string", $response);
			     $myfile = fopen("2-oct-2016.xml", "w") or die("Unable to open file!");
					fwrite($myfile, $response);
					fclose($myfile);exit();
				$xmlObj=simplexml_load_string($response);
				$simpleArray=json_decode(json_encode($xmlObj),true);
				$this->generateArray($simpleArray);
				return $this->myarray;
		}	
	}

	function parseXML(){
		$retArray=array();
		if(isset($this->xmlResponse->params->param->value->array[0]->data->value)){
			for($i=0;$i<count($this->xmlResponse->params->param->value->array->data->value);$i++){
				if(isset($this->xmlResponse->params->param->value->array->data->value[$i]->struct->member)){
					for($j=0;$j<count($this->xmlResponse->params->param->value->array->data->value[$i]->struct->member);$j++){
						if(isset($this->xmlResponse->params->param->value->array->data->value[$i]->struct->member[$j]->name)){	
							$retArray[$i][trim($this->xmlResponse->params->param->value->array->data->value[$i]->struct->member[$j]->name)]=(string)$this->xmlResponse->params->param->value->array->data->value[$i]->struct->member[$j]->value->string;
						}	
					}
				}
			}
		}
		return $retArray;
	}
	function parseMenu(){
		if(isset($this->xmlResponse->params->param->value->string)){
			$menu=(string)$this->xmlResponse->params->param->value->string;
			$menu=str_replace("sub-menu","level2",$menu);
			return $menu;
		}
	}
}
