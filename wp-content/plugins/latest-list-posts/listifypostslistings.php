<?php
/*
Plugin Name: Latest Listing Posts
Plugin URI: http://www.indapoint.com
Description: This plugin is made for shown latest listings list from the listing website to this news website
Author: Bhavesh Akhade
Version: 1
Author URI: http://aumwebtechs.com/
*/


class ListifypostsListings extends WP_Widget
{
  function ListifypostsListings()
  {
    $widget_ops = array('classname' => 'ListifypostsListings', 'description' => 'Displays a Latest Listing Posts' );
    $this->WP_Widget('ListifypostsListings', 'Latest Listing Posts', $widget_ops);
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
    $title = $instance['title'];
?>
  <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);
 
    echo $before_widget;
    $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
 
    if (!empty($title))
      echo $before_title . $title . $after_title;;
 
    // WIDGET CODE GOES HERE
    include_once ("wpblog.cls.php");
   $wpObj=new wpblog(ODB_USER,ODB_PASSWORD,OURL);
    $RecentpostDetails = $wpObj->getBlogDetails();
  
    for ($i = 0; $i < count($RecentpostDetails) - 1; $i++)
        {
        $postdate = explode('T', $RecentpostDetails[$i]['post_date']);
        $date = date_create($postdate[0]);
        $formatedDate = date_format($date, "M d, Y");
        echo '<div class="listing-live-details">';
        if ($RecentpostDetails['post_thumbnail'] != '')
            {
            $thumnail = '<img src="' . $RecentpostDetails[$i]['post_thumbnail'] . '">';
            }
        elseif ($RecentpostDetails[$i]['thumbnail'] != '')
            {
            $thumnail = '<img src="' . $RecentpostDetails[$i]['thumbnail'] . '">';
            }
          else
            {
            $thumnail = '<img src="';
    $thumnail .= get_site_url();
    $thumnail .= '/wp-content/uploads/2016/07/noimage_th.png" />';
            }

        echo '<div class="listing-live-images cb-mask cb-img-fw">';
        echo '<a href="' . $RecentpostDetails[$i]['guid'] . '">' . $thumnail . '</a>';
        echo '</div>';
        echo '<div class="listing-live-textes">';
        echo '<p><strong><a href="' . $RecentpostDetails[$i]['guid'] . '">' . $RecentpostDetails[$i]['post_title'] . '</a></strong></p>';
        echo '<p>' . $formatedDate . '</p>';
        echo '</div></div>';
        }
 
    echo $after_widget;
  }
 
}
add_action( 'widgets_init', create_function('', 'return register_widget("ListifypostsListings");') );?>